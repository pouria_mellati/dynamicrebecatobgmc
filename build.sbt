organization := "org.rebeca.lang"

name := "DynamicRebecaToBgmc"

moduleName := "DynamicRebecaToBgmc"

version := "0.1.1-SNAPSHOT"

scalaVersion := "2.10.2"

// Prevent the eclipse plugin from creating src/main/java and src/test/java.

unmanagedSourceDirectories in Compile <<= (scalaSource in Compile)(Seq(_))

unmanagedSourceDirectories in Test <<= (scalaSource in Test)(Seq(_))


// Ensure that the local mvn repository is globally added to sbt as a resolver.

libraryDependencies += "org.rebecalang" % "org.rebecalang.dynamiccompiler" % "2.3.0-SNAPSHOT"
