package org.rebecalang.bgmcgen

import TranslationUtils._
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel._
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.ParametrizedType
import scala.collection.JavaConversions._
import scala.collection.mutable.Buffer

class ReactiveClassTranslator(rcd: ReactiveClassDeclaration, packageName: String) {
  val fieldInfos = Buffer.empty[FieldInfo]
  
  def xlate = {
    s"""
package $packageName;

import utils.*;
import java.util.*;
import bgmc.ObjectVal;
import bgmc.actormodelhelpers.*;
import bgmc.actormodelhelpers.actordefinition.*;

public class ${rcd.getName} extends AbstractActorState {
	public static int maxQLength = ${rcd.getQueueSize};
	public static String classId = "${rcd.getName}";
	
	private static int instancesCount = 0;
	public static String newId() {
		instancesCount += 1;
		return classId + "_instance_" + instancesCount;
	}
	
	$xlateStatevars
	
	public ${rcd.getName}() {
		super(maxQLength, classId);
	}
	
	@Override
	public List<ActorStateSuccessorDelta> getSuccessorDeltas(String self) {
		List<ActorStateSuccessorDelta> deltas = new LinkedList<>();
		Message msg = messageQ.peek();	// We can only peek now, since we cannot mutate an ActorState.
		
		${rcd.getMsgsrvs.map {
		  new MsgsrvXlator(_, rcd, fieldInfos).xlate
		} :+ """throw new RuntimeException("Actor of class " + classId + " received an unsupported message: " + msg.name);""" mkString "\n\t\telse "}
			
		
		return deltas;
	}
	
	@Override
	public ObjectVal getNonActorField(String fieldName) {
		${fieldInfos.filterNot{_._type.isActorRef}.map {f =>
		  s"""if(fieldName.equals("${f.name}")) return new SimpleObjectVal("${f._type.name}", this.${f.name});"""
		} :+ """throw new IllegalArgumentException("ReactiveClass " + getClassId() + " has no such field: " + fieldName);""" mkString "\n\t\telse "} 
	}
	
	@Override
	public String tryGetActorFieldId(String fieldName) {
		${fieldInfos.filter{_._type.isActorRef}.map {f =>
		  s"""if(fieldName.equals("${f.name}")) return this.${f.name};"""
		} :+ "return null;" mkString "\n\t\telse "}
	}
	
	@Override
	protected boolean actorEquals(Object thatObj) {
		${rcd.getName} that = (${rcd.getName})thatObj;
		
		return ${
		  (fieldInfos.filter{_._type.isPrimitive}.map{f => 
		    s"(this.${f.name} == that.${f.name})"} ++
		  fieldInfos.filterNot{_._type.isPrimitive}.map{f =>
		    s"(this.${f.name} == null ? that.${f.name} == null : this.${f.name}.equals(that.${f.name}))"}).
		  mkString(" &&\n\t\t\t")
		};
	}
		
	@Override
	protected int actorHashCode() {
		final int prime = 31;
		int result = 1;
		
		${
		  fieldInfos.filterNot{_._type.isPrimitive}.map{f => s"result = prime * result + ((${f.name} == null) ? 0 : ${f.name}.hashCode());"} ++
		  fieldInfos.filter{_._type.isPrimitive}.map{f =>
		    if(f._type.name == "boolean") s"result = prime * result + (${f.name} ? 1231 : 1237);"
		    else /* Its a numeric type. */ s"result = prime * result + ${f.name};"
		  } mkString "\n\t\t"
		}
		
		return result;
	}
	
	${
	  val deepCopiables = fieldInfos.filter{_._type.requiresDeepCopy}
	  if(deepCopiables.size > 0)
    s"""
    @Override
    public AbstractActorState clone() {
		${rcd.getName} c = (${rcd.getName}) super.clone();
		
		${deepCopiables.map{f =>
		  if(f._type.name matches """List.*<.+>\s*""")
		    s"""if(this.${f.name} != null)
		  		c.${f.name} = new LinkedList<>(this.${f.name});"""
		  else
		    throw new TranslationError(s"Unable to generate deep copying code for field:\n$f.")
		} mkString "\n"}
		
		return c;
	}
    """ else ""}
}
    """
  }
  
  def xlateStatevars: String = {    
    rcd.getStatevars.map { f =>
      val translator = new FieldXlator(f)(nullXlationScope)
      val translation = translator.xlate
      fieldInfos ++= translator.fieldInfos
      translation
    } mkString "\n\t"
  }
}

case class FieldInfo(_type: TypeInfo, name: String)
case class TypeInfo(name: String, isActorRef: Boolean, isPrimitive: Boolean, requiresDeepCopy: Boolean)	// TODO: Rename name to javaName.

class FieldXlator(f: FieldDeclaration)(implicit scope: XlationScope) {
  val fieldInfos = Buffer.empty[FieldInfo]
  
  def xlate: String = {
    val typeInfo = new TypeXlator(f.getType).xlate
    
    f.getVariableDeclarators.map { vd =>
      fieldInfos += FieldInfo(typeInfo, vd.getVariableName)
      
      implicit val precedingStmts = Buffer.empty[String]
      val initializerStr = new VariableInitializerXlator(vd.getVariableInitializer).xlate
      
      (precedingStmts :+ 
      s"${typeInfo.name} ${scope.nameLocalizer(vd.getVariableName)} ${if(initializerStr.isDefined) s"= ${initializerStr.get}" else ""};") mkString "\n"
    } mkString "\n"
  }
}

class TypeXlator(t: Type/*, useBoxedVersions: Boolean = false*/) {
  val supportedParametrizedTypesToJava = Map("list" -> "List")
      
  def xlate: TypeInfo =
    t match {
      case ot: OrdinaryPrimitiveType =>
        if(supportedPrimitivesToBoxed contains ot.getName)
          //if(useBoxedVersions)
            TypeInfo(supportedPrimitivesToBoxed(ot.getName), isActorRef = false, isPrimitive = false, requiresDeepCopy = false)
          //else
          //  TypeInfo(ot.getName, isActorRef = false, isPrimitive = true, requiresDeepCopy = false)
        else
          TypeInfo("String", isActorRef = true, isPrimitive = false, requiresDeepCopy = false)
      case pt: ParametrizedType =>
        val typeName = s"""${supportedParametrizedTypesToJava(pt.getName)} <${pt.getParameters.map {
      		    new TypeXlator(_/*, true*/).xlate.name
      		  } mkString ", "
      		}>"""  	
        TypeInfo(typeName, isActorRef = false, isPrimitive = false, requiresDeepCopy = true)
      case unsupported: Type =>
        throw new TranslationError(s"Unsupported field type encountered at line ${unsupported.getLineNumber}.")
    }
}

class VariableInitializerXlator(vi: VariableInitializer)(implicit scope: XlationScope, precedingStmts: Buffer[String]) {
  def xlate: Option[String] = vi match {
    case oi: OrdinaryVariableInitializer => if(oi.getValue == null) None else Some(new ExprXlator(oi.getValue).xlate)
    case null => None
    case unsupported: VariableInitializer => throw new TranslationError(s"Unsupported variable initializer encountered at line ${unsupported.getLineNumber}.")
  }
}

// Should translate a msgsrv to the following form:
// if(the msg belongs to this msgsrv) {... handle ...}
class MsgsrvXlator(ms: MsgsrvDeclaration, rcd: ReactiveClassDeclaration, statevars: Iterable[FieldInfo]) {
  
  // Calculates the crossProduct of a bunch of Seqs.
  def crossProduct[T](seqs :Seq[Seq[T]]): Seq[Seq[T]] = seqs match {
    case Seq(l) => l map {Seq(_)}
    case Seq(l, ls @ _*) =>
    	for(elem <- l; crossRest <- crossProduct(ls)) yield Seq(elem) ++ crossRest
  }
    
  def xlate: String =	// TODO: Get rid of name localization for non-det alternatives by using unnamed blocks.
    s"""
    if(msg.name.equals("${ms.getName}")) {
  		String sender = (String)msg.data.get(0); 
    	${ms.getFormalParameters.zipWithIndex.map{case (p, index) =>
    	  val paramTypeStr: String = new TypeXlator(p.getType).xlate.name
    	  s"""$paramTypeStr ${p.getName} = ($paramTypeStr)msg.data.get(${index + 1});"""
    	} mkString "\n"}
    	
  		${
  		  // Added to ensure that the msgsrv is run at least once, even if there are no nonDetExprs in this msgsrv.
  		  val nonExistantNonDet = new NonDetExpression
  		  nonExistantNonDet.getChoices.add(new Expression)
  		  
  		  val nonDetExprs = (allNonDetExprsIn(ms.getBlock.getStatements) filter {_.getChoices.size > 0}) :+ nonExistantNonDet
  		  val nonDetChoiceCombinations = crossProduct(nonDetExprs.map{_.getChoices.toSeq})
  		  nonDetChoiceCombinations.zipWithIndex.map {case (choiceCombination, combinationNum) =>
  		    val nonDetExpr2Choice = (nonDetExprs zip choiceCombination).toMap[NonDetExpression, Expression]
  		    //val loc = {nameToLocalize: String => "_" + nameToLocalize + s"_alt_$combinationNum"}
  		    val loc = {name: String => name}
  		    
  		    implicit val scope = XlationScope(nonDetExpr2Choice, loc, statevars)
  		    
  		    s"""{
		  		${rcd.getName} ${loc("my")} = (${rcd.getName})this.clone();
				${loc("my")}.messageQ.remove();
				
				MessageAggregator ${loc("messenger")} = new MessageAggregator();
				Map<String, ActorState> ${loc("createdActors")} = new HashMap<String, ActorState>(0);
				
				${new StatementXlator(ms.getBlock).xlate}
				
				deltas.add(new ActorStateSuccessorDelta(${loc("my")}, ${loc("createdActors")}, ${loc("messenger")}.sentMessages));
				}
  		    """
  		    
  		  } mkString "\n\n"
  		}
	}
    """
  
  // Finds all instances of NonDetExpression in a set of statements by reflectively following the chain of getters on these statements.
  // TODO: Needs a better implementation: It may be possible to have non-statements leading to statements.
  def allNonDetExprsIn(stmts: Iterable[Statement]): Seq[NonDetExpression] = {
    import collection.mutable.Set
    val nonDets = Set.empty[NonDetExpression]
    val visitedStatements = Set.empty[Statement]	// To avoid infinite loops in case of circular accessibility between statements.
    
    def findNonDets(st: Statement) {
      if(st == null || visitedStatements.contains(st)) return
      visitedStatements += st
      
      if(st.isInstanceOf[NonDetExpression])
        nonDets += st.asInstanceOf[NonDetExpression]
      else {
        st.getClass.getMethods.filter{m =>
          m.getName.startsWith("get") && m.getParameterTypes.size == 0 && classOf[Statement].isAssignableFrom(m.getReturnType)}.foreach{m =>
            val gotStatement = m.invoke(st)
            if(gotStatement != null) findNonDets(gotStatement.asInstanceOf[Statement])
          }
        
        // TODO: Specifically written against BlockStatement. Fragile.
        if(st.isInstanceOf[BlockStatement])
          st.asInstanceOf[BlockStatement].getStatements.foreach(findNonDets(_))
      }
    }
    
    stmts.foreach{findNonDets(_)}
    nonDets.toSeq
  }
}