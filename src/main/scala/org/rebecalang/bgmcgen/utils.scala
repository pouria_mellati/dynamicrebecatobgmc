package org.rebecalang.bgmcgen

import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel._
import scala.collection.mutable.Buffer

case class XlationScope(nonDetMap: Map[NonDetExpression, Expression], nameLocalizer: String => String, stateVars: Iterable[FieldInfo]) { 	// TODO: Rename nameLocalizer to localizeName.
  private var tempVarIndex = 0
  
  def newUniqVarName = {
    tempVarIndex += 1
    nameLocalizer(s"temp_var_$tempVarIndex")
  }
}

object TranslationUtils {
  // Creates an in-place expression for a sequence of statements that finally return a result.
//  def asMultiStatementExpression(statementsThatReturnResult: String, resultTypeStr: String)(implicit scope: XlationScope): String =
//    s"""(new MultiStatementExpr<$resultTypeStr> () {@Override public $resultTypeStr get() {$statementsThatReturnResult}}.get())"""
  
  // TODO: Only works with java 7 and when java 7 type inference is applicable. Needs fixing.
  def javaAbstractCollTypeStr2ConcreteType(abstractTypeStr: String) =
    if(abstractTypeStr matches """List.*<.+>\s*""") "LinkedList<>"
    else throw new TranslationError(s"Cannot find concrete type for abstract generic type $abstractTypeStr.")
  
  def stmtsForMsgSend(recipientId: String, msgName: String, params: Seq[Expression])(implicit scope: XlationScope): String =
    s"""ArrayList<Object> msgData = new ArrayList<Object>(${params.size + 1});
    msgData.add(self);
    ${params.map{p =>
      implicit val precedingStatements = Buffer.empty[String]
      val mainStmt = s"msgData.add(${new ExprXlator(p).xlate});"
      (precedingStatements :+ mainStmt) mkString "\n"
    } mkString "\n"}
    ${scope.nameLocalizer("messenger")}.send($recipientId, new Message("$msgName", msgData));
    """
  
  lazy val nullXlationScope = XlationScope(Map.empty[NonDetExpression, Expression], {name: String => name}, Seq.empty[FieldInfo])
  
  val supportedPrimitivesToBoxed = Map(
      "int" -> "Integer",
      "byte" -> "Byte",
      "short" -> "Short",
      "boolean" -> "Boolean")
}