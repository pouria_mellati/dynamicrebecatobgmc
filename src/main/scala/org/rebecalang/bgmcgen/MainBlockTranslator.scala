package org.rebecalang.bgmcgen

import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.BlockStatement

class MainBlockTranslator(mainBlock: BlockStatement, className: String, packageName: String) {
  def xlate: String =
s"""
package $packageName;

import java.util.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import utils.*;
import bgmc.*;
//import bgmc.ObjectVal;
import bgmc.actormodelhelpers.*;
import bgmc.actormodelhelpers.actordefinition.*;
import bgmc.property.Formula;
import bgmc.property.LtlQAwareBaState;
import bgmc.property.parser.PropertyParser;

public class $className {
	// The first arg to the app is the path to the property file.
	public static void main(String[] args) throws FileNotFoundException {
		if(args.length < 1)
			throw new RuntimeException("The only arg to the app (the path to the property file) was not specified.");
		
		MessageAggregator messenger = new MessageAggregator();
		Map<String, ActorState> createdActors = new HashMap<>(2);
		String self = "$className";

		${new StatementXlator(mainBlock)(TranslationUtils.nullXlationScope).xlate}
  
  		// Manually add any sent messages to the created actors.
  		for(Map.Entry<String, ActorState> actorKV: createdActors.entrySet())
			if(messenger.sentMessages.containsKey(actorKV.getKey()))
				for(Message msg: messenger.sentMessages.get(actorKV.getKey()))
					((AbstractActorState)actorKV.getValue()).addMessage(msg);
  
  		List<ActorModelState> initialStates = new LinkedList<>();
		initialStates.add(new ActorModelState(createdActors, null));
		ActorModel model = new ActorModel(initialStates);
  		
  		FileReader propertyFile = new FileReader(args[0]);
		Formula propertyFormula = new PropertyParser().parsePropertyAndNegate(propertyFile);
		System.out.println("The negation of the property is:\\n" + propertyFormula);
		BaState initPropertyState = LtlQAwareBaState.fromFormula(propertyFormula);
		
		System.out.println("Running the model checker ...");
		long milisBeforeRun = System.currentTimeMillis();
		ModelCheckingResult result = new ModelChecking(model, initPropertyState).run();
		System.out.println("Took " + (System.currentTimeMillis() - milisBeforeRun) / 1000.0 + " secs.");
		if(result.wasModelValid())
			System.out.println("The model is valid!");
		else {
			System.out.println("The model was invalid!");
		}
	}
}
"""
}