package org.rebecalang.bgmcgen

import org.rebecalang.compiler.modelcompiler.dynamicrebeca.DynamicRebeca2Ast
import scala.collection.JavaConversions._
import java.io.PrintWriter
import java.io.File
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.extratypes.MainBlock

object Main {
  def main(args: Array[String]): Unit = {
    val (pathToRebecaFile, outputDir) = (args(0), args(1))
    
    val rebecaModel = DynamicRebeca2Ast fromRebecaFileAt pathToRebecaFile
    val modelPackageName = "rebecamodel"
    
    val / = File.separator
    
    val mainBlock = rebecaModel.getRebecaCode.getMainDeclaration.asInstanceOf[MainBlock].getBlock
    val mainClassName = "_Main"
    writeInFileAndMkDir(outputDir + / + "src" + / + "main" + / +"java"+ / + modelPackageName + / + s"$mainClassName.java") {
      _.println(new MainBlockTranslator(mainBlock, mainClassName, modelPackageName).xlate)
    }
    
    for(rcd <- rebecaModel.getRebecaCode.getReactiveClassDeclaration)
      writeInFileAndMkDir(outputDir + / + "src" + / + "main" + / +"java"+ / + modelPackageName + / + rcd.getName + ".java") {
      	_.println(new ReactiveClassTranslator(rcd, modelPackageName).xlate)  
      }
  }
  
  private def writeInFileAndMkDir(pathToFile: String)(writes: PrintWriter => Unit) {
    val file = new File(pathToFile)
    file.getParentFile.mkdirs
    val writer = new PrintWriter(file, "UTF-8")
	writes(writer)
	writer.close
  }
}