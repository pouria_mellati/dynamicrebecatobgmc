package org.rebecalang.bgmcgen

import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel._
import collection.JavaConversions._
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.ForeachStatement
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.CollectionLiteral
import TranslationUtils._
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.ParametrizedType
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.NewExpression
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.MsgSendStatement
import scala.collection.mutable.Buffer

class StatementXlator(st: Statement)(implicit scope: XlationScope) {
  
  private implicit def statement2translatable(st: Statement) = new {
    def xlate = new StatementXlator(st).xlate
  }
  
  // Re-uses a precedingStmts from its scope.
  private implicit def expr2translatable(st: Expression)(implicit precedingStmts: Buffer[String]) = new {
    def xlateE = new ExprXlator(st).xlate
  }
  
  def xlate: String = {
    implicit val precedingStmts = Buffer.empty[String]
    val mainStatement = st match {
      case null => ""
      case fd: FieldDeclaration => new FieldXlator(fd).xlate
      case expr: Expression => new ExprXlator(expr).xlate
      case otherStmt => xlateOther
    }
    (precedingStmts :+ mainStatement) mkString "\n"
  }
  
  private def xlateOther(implicit precedingStmts: Buffer[String]): String = st match {
    case b: BlockStatement =>
      s"""{
      	${b.getStatements.map{s => s"${s.xlate};"} mkString "\n"}
      }"""
    case cs: ConditionalStatement =>
      s"""if(${cs.getCondition.xlateE})
      	${cs.getStatement.xlate}
      	${if(cs.getElseStatement != null) s"else ${cs.getElseStatement.xlate}" else ""}
      """
    case ws: WhileStatement =>
      s"""while(${ws.getCondition.xlateE}) ${ws.getStatement.xlate}
      """
    case fes: ForeachStatement =>
      s"""for(${new TypeXlator(fes.getVarType).xlate.name} ${fes.getVarName}: ${fes.getRange.xlateE})
      	${fes.getStatement.xlate}
      """
    case fs: ForStatement =>
      val forInitXlation = if(fs.getForInitializer.getFieldDeclaration != null)
    	  new FieldXlator(fs.getForInitializer.getFieldDeclaration).xlate
        else if(fs.getForInitializer.getExpressions != null)
          fs.getForInitializer.getExpressions.map{_.xlateE}.mkString(", ") + ";"
        else ";"
          
      s"""for($forInitXlation ${fs.getCondition.xlateE}; ${fs.getForIncrement.map{_.xlate} mkString ", "})
      ${fs.getStatement.xlate}
      """
    case ms: MsgSendStatement =>
      s"""{
      	${stmtsForMsgSend(new ExprXlator(ms.getRecipient).xlate, ms.getMessageName, ms.getParameters)}
      }"""
    case rs: ReturnStatement => s"return ${rs.getExpression.xlateE};"
    case cs: ContinueStatement => "continue;"
    case br: BreakStatement => "break;"
    case ss: SwitchStatement =>
      throw new TranslationError("The switch statement is not supported yet.")
    case unsupported: Statement =>
      throw new TranslationError(s"Unable to translate unsupported statement of type ${unsupported.getClass}.")
  }
}

class ExprXlator(e: Expression)(implicit scope: XlationScope, precedingStatements: Buffer[String]) {
  private implicit def expr2translatable(expr: Expression) = new {
    def xlate = new ExprXlator(expr).xlate
  }
  
  def xlate: String = e match {
    case ue: UnaryExpression =>
      s"""(${ue.getOperator}${ue.getExpression.xlate})"""
  	case be: BinaryExpression =>
  	  def xlateEqEq =
  	    s"(Object)${be.getLeft.xlate} == null ? (Object)${be.getRight.xlate} == null : ((Object)${be.getLeft.xlate}).equals(${be.getRight.xlate})"		// TODO: The left expr is evaluated twice.
  	  val operationCode =
  	    if(be.getOperator == "==")
  	      xlateEqEq
  	    else if(be.getOperator == "!=")
  	      s"! ($xlateEqEq)"
  	    else
  	      s"${be.getLeft.xlate} ${be.getOperator} ${be.getRight.xlate}"
  	  if(be.getOperator.endsWith("=") && be.getOperator != "==" && be.getOperator != "!=") operationCode else s"($operationCode)"
    case te: TernaryExpression =>
      s"""((${te.getCondition.xlate}) ? (${te.getLeft.xlate}) : (${te.getRight.xlate}))"""
    case pp: PlusSubExpression => // Seems to be either a ++ or -- posfix.
      s"""${pp.getValue.xlate} ${pp.getOperator}"""
    case ce: CastExpression =>
      s"""((${new TypeXlator(ce.getType).xlate}) ${ce.getExpression.xlate})"""
    case ne: NonDetExpression =>
      scope.nonDetMap(ne).xlate
    case cl: CollectionLiteral =>
      val typeInfo = new TypeXlator(cl.getType).xlate
      val collConcreteTypeStr = javaAbstractCollTypeStr2ConcreteType(typeInfo.name)
      
      val createdCollName = scope.newUniqVarName
      
      precedingStatements += s"""${typeInfo.name} $createdCollName = new $collConcreteTypeStr();"""
      precedingStatements ++= cl.getElemExprs.map{elem => s"""$createdCollName.add(${elem.xlate});"""}
      
      createdCollName
    case l:  Literal =>
      l.getLiteralValue
    case ne: NewExpression =>	// TODO: Do not send the initial message if the reactiveclass does not define one.
      if(! new TypeXlator(ne.getType).xlate.isActorRef)
        throw new TranslationError("Keyword 'new' is used to create a non-reactiveclass type.")
      
      val actorStateTypeStr = ne.getType.asInstanceOf[OrdinaryPrimitiveType].getName
      
      val newActorStateVarName = scope.newUniqVarName
      val newActorIdVarName	   = scope.newUniqVarName
      
      precedingStatements ++= Seq(
          s"""$actorStateTypeStr $newActorStateVarName = new $actorStateTypeStr();""",
          s"""String $newActorIdVarName = $actorStateTypeStr.newId();""",
          s"""{ArrayList<Object> msgData = new ArrayList<Object>(${ne.getParamExprs.size + 1});
		  msgData.add(self);
		  ${ne.getParamExprs.map{p =>
		    val precedingStmts = Buffer.empty[String]
		    val mainStmt = s"msgData.add(${new ExprXlator(p)(scope, precedingStmts).xlate});"
		    (precedingStmts :+ mainStmt) mkString "\n"
		  } mkString "\n"}
		  
		  $newActorStateVarName.addMessage(new Message("initial", msgData));
		  List<ActorStateSuccessorDelta> succs = $newActorStateVarName.getSuccessorDeltas($newActorIdVarName);
		  if(succs.size() != 1) throw new RuntimeException("The num successors of the initial message is more than 1!");
		  
		  ActorStateSuccessorDelta succ = succs.get(0);
		  ${scope.nameLocalizer("createdActors")}.put($newActorIdVarName, succ.getActorStateAfterRunningMsgHandler());
		  ${scope.nameLocalizer("createdActors")}.putAll(succ.getNewlyInstantiatedActors());
		  ${scope.nameLocalizer("messenger")}.sendAll(succ.getSentMessages());}"""
      )
      
      newActorIdVarName
    case dp: DotPrimary =>
      s"""${dp.getLeft.xlate}.${dp.getRight.xlate}"""
    case tp: TermPrimary =>
      var result = if(scope.stateVars.map{_.name} contains tp.getName) s"my.${tp.getName}" else s"${tp.getName}" 
      if(tp.getParentSuffixPrimary != null)
        result += s"(${tp.getParentSuffixPrimary.getArguments.map{_.xlate}.mkString(", ")})"
      result += tp.getIndices.map{e => s".get(${e.xlate})"}.mkString	// TODO: Ensure the term is not an array before translating [i]'s to .get(i)'s.
      result
    case unsupported: Expression =>
      throw new TranslationError(s"Unable to translate unsupported Expression of type ${unsupported.getClass}.")
  }
}