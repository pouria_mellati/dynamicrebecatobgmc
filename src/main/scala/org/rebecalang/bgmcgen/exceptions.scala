package org.rebecalang.bgmcgen

class TranslationError(msg: String) extends RuntimeException(msg)